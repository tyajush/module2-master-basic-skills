Face recognition uses the deep neural networks to implement. Once the deep learning algorithm gains experience using the accumulated past and new large datasets, any technology employing deep learning benefits can make reliable predictions and also present factual responses to real-time data.
Face recognition have 4 basic steps:- 
1. face detection , means locate faces in image based on features like eye mouth lips etc.
2. face alignment , means normalize the face to be consistent with database.
3. features extraction , extract features from face to recognize specific person stored in database.
4. face Recognition. Perform matching of the face against one or more known faces which are stored in database.

This block diagram will make it more clear:-

![Overview-of-the-Steps-in-a-Face-Recognition-Process-1024x362](/uploads/fcf79b0c8ce6ee02b9ebf8cdb75faa20/Overview-of-the-Steps-in-a-Face-Recognition-Process-1024x362.png)

The first step in a broader face recognition system, face detection must be robust. For example, a face cannot be recognized if it cannot first be detected. That means faces must be detected with all manner of orientations, angles, light levels, hairstyles, hats, glasses, facial hair, makeup, ages, and so on.

**Face recognition technique used in deep learning**
- This technique is known as deep metric learning. In deep metric learning , instead of trying to output a single label, we are instead outputting a real-valued feature vector. The output feature vector is 128-d.
- To find face we convert image in black and white picture and then scan single pixel and then check the surrounding pixels whether they are dark or light and in which direction they are getting darker.
- Arrow is applied in that direction and whole image is converted to dark pixels, which helps in easily detecting the face features.
- These features are later on matched with the features stored in database to recognize the face.


![np](/uploads/91d6a20356dc1ff378555231546b8022/np.jpeg)

![npbg](/uploads/35b820ab82c2436d6c0a4ea56808abc1/npbg.png)